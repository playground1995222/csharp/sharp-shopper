namespace sharp_shopper;

class Customer
{
    string email {get;}
    string name {get;}
    List<Order> orders {get;}

    public Customer(string email, string name)
    {
        this.email = email;
        this.name = name;
        orders = [];
    }
}