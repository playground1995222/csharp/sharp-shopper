namespace sharp_shopper;

class Order
{
    public int orderId {get;}
    public DateTime dateTime {get;}
    public List<Product> products {get;}

    public Order(int orderId, DateTime dateTime)
    {
        this.orderId = orderId;
        this.dateTime = dateTime;
        products = [];
    }

    public void AddProduct(Product product) {
        products.Add(product);
    }
}