namespace sharp_shopper;

public class Electronic : Product
{
    string brand;

    public Electronic(string name, decimal price, string brand) : base(name, price)
    {
        this.brand = brand;
    }

    public override void DisplayInfo()
    {
        System.Console.WriteLine($"name: {name}, price: {price:C}, brank: {brand}");
    }
}