namespace sharp_shopper;

public abstract class Product
{
    public string name { get; set; }
    public decimal price { get; set; }

    protected Product(string name, decimal price)
    {
        this.name = name;
        this.price = price;
    }

    public abstract void DisplayInfo();

    // TODO: statisk metod för rabatt
}